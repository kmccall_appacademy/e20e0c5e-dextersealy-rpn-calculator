class RPNCalculator

  def initialize
    @stack = []
  end

  def value
    @stack[-1]
  end

  def push(num)
    @stack << num
  end

  def plus
    lhs, rhs = pop2
    push(lhs + rhs)
  end

  def minus
    lhs, rhs = pop2
    push(lhs - rhs)
  end

  def divide
    lhs, rhs = pop2
    push(lhs.to_f / rhs)
  end

  def times
    lhs, rhs = pop2
    push(lhs * rhs)
  end

  def tokens(string)
    string.split.map do |token|
      case token
      when "*"
        :*
      when "+"
        :+
      when "-"
        :-
      when "/"
        :/
      else
        token.to_f
      end
    end
  end

  def evaluate(string)
    tokens(string).each do |token|
      case token
      when :*
        times
      when :+
        plus
      when :-
        minus
      when :/
        divide
      else
        push(token)
      end
    end

    value
  end

  private

  def pop2
    raise "calculator is empty" if @stack.length < 2
    rhs, lhs = @stack.pop, @stack.pop
    return lhs, rhs
  end

end
